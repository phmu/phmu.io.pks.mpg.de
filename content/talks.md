---
title: "Talks"
date: "2017-06-10T17:39:21-07:00"
draft: true
---

# Talks

- [Dynamics Days 2016](phmu.io.pks.mpg.de/talk-dynamics-days-2016)
- [How much computer skills does a physicist needs?](phmu.io.pks.mpg.de/talk-how-much-computer-skills)
- [DPG spring meeting 2017](phmu.io.pks.mpg.de/talk-DPG-spring-meeting-2017)
- [EVA 2017](phmu.io.pks.mpg.de/talk-eva-2017)
- [Power law distributions in empirical data](phmu.io.pks.mpg.de/talk-power-law-fit)

