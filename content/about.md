---
title: "About"
date: "2017-06-27T17:39:21-07:00"
---

This is an example website to demonstrate how to create a neat page
using just Markdown, [Hugo](https://gohugo.io/), and [GitLab
Pages](https://docs.gitlab.com/ee/user/project/pages/) (or Github, as
you wish).
