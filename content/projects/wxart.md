---
title: "wxArt"
date: "2016-03-15T17:39:21-07:00"
description: "https://github.com/jusjusjus/wxArt"
---

A Python interface to perform a style transfer between images using
artificial intelligence and deep neuronal networks.
