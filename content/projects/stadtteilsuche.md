---
title: "Stadtteilsuche"
date: "2016-11-10T17:39:21-07:00"
description: "https://github.com/theGreatWhiteShark/StadtteilsucheDD"
---

Which part of town fits you most? 

The award-winning web app by Stephan, Justus, and me at a Hackathon in 2016.
