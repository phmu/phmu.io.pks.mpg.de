---
title: "Dynamics Days 2016: Robustness and reliability of fitting
extreme value distributions"
date: "2016-06-10T17:39:21-07:00"
description: "http://phmu.io.pks.mpg.de/talk-dynamics-days-2016"
---

My first talk prepared using reveal.js. That's why it's that well
done. So, for a proper introduction into this technology please see
the source code of one of the other talks.
