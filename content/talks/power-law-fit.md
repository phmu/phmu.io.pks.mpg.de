---
title: "Power-law distributions in empirical data"
date: "2017-12-12T17:39:21-07:00"
description: "http://phmu.io.pks.mpg.de/talk-power-law-fit"
---

In this talk I provided insights into the paper [Power-law
distributions in empirical data](https://arxiv.org/abs/0706.1062) by
Aaron Clauset, Cosma Rohilla Shalizi, and M. E. J. Newman
